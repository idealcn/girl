package com.girl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class GirlController {

    @Autowired
    private GirlRepository girlRepository;

    @GetMapping(value = "/girlList")
    public List<Girl> girlList(){
      return   girlRepository.findAll();
    }

    @PostMapping(value = "/addGirl")
    public Girl add(@RequestParam("age") Integer age,@RequestParam("cupSize") String cupSize){
        Girl girl = new Girl();
        girl.setAge(age);
        girl.setCupSize(cupSize);
        return girlRepository.save(girl);
    }

    @GetMapping(value = "/queryGirl/{id}")
    public Girl queryGirl(@PathVariable("id") Integer id){
       return girlRepository.findOne(id);
    }

}
